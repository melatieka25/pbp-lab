from django.db import models

# Create your models here.
class Note(models.Model):
    msg_date = models.DateField()
    msg_to = models.CharField(max_length=30)
    msg_from = models.CharField(max_length=30)
    msg_title = models.CharField(max_length=30)
    msg = models.CharField(max_length=100)


