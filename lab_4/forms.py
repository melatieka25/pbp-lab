from lab_2.models import Note
from django import forms  

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['msg_date', 'msg_to', 'msg_from', 'msg_title', 'msg']