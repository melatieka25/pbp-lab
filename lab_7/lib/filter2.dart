import 'package:flutter/material.dart';

// https://api.flutter.dev/flutter/material/FilterChip-class.html
class CategoryFilterEntry {
  const CategoryFilterEntry(this.name, this.value, this.color);
  final String name;
  final String value;
  final Color color;
}

class CastFilter extends StatefulWidget {
  const CastFilter({Key? key}) : super(key: key);

  @override
  State createState() => CastFilterState();
}

class CastFilterState extends State<CastFilter> {
  final List<CategoryFilterEntry> _cast = <CategoryFilterEntry>[
    const CategoryFilterEntry('Rapid', '1', Colors.red),
    const CategoryFilterEntry('Swab Antigen', '2', Colors.yellow),
    const CategoryFilterEntry('PCR', '3', Colors.blue),
    const CategoryFilterEntry('PCR Drive Thru', '4', Colors.green),
    const CategoryFilterEntry('Tes Serologi', '5', Colors.purple),
  ];
  final List<String> _filters = <String>[];

  Iterable<Widget> get categoryWidgets sync* {
    for (final CategoryFilterEntry category in _cast) {
      yield Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: FilterChip(
          avatar: CircleAvatar(radius: 10, backgroundColor: category.color,),
          label: Text(category.name),
          selected: _filters.contains(category.name),
          onSelected: (bool value) {
            setState(() {
              if (value) {
                _filters.add(category.name);
              } else {
                _filters.removeWhere((String name) {
                  return name == category.name;
                });
              }
            });
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const SizedBox(height: 20),
        Wrap(
          children: categoryWidgets.toList(),
        ),
        Text((() {
          if(_filters.isNotEmpty){
            return "Hasil filter: ${_filters.join(', ')}";}
          return "";
        })()),
        SizedBox(height: (() {
          if(_filters.isNotEmpty){
            return 20.0;}
          return 0.0;
        })()),
      ],
    );
  }
}