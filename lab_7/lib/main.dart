import 'package:flutter/material.dart';
import 'package:lab_7/tes_covid.dart';
import 'package:lab_7/filter2.dart';
import 'package:lab_7/tambah_tes.dart';

void main() => runApp(MyApp());

List<Widget> list = [
  const Card(
    child: CastFilter(),
  ),
];

List<Widget> list2 = covidTest.map((covid) => covidTestTemplate(covid))
    .toList();



/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        home: TesCovidScreen());
  }
}

class TesCovidScreen extends StatelessWidget {
  const TesCovidScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // https://flutter.dev/docs/development/ui/assets-and-images
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.light,
        primaryColor: Colors.lightBlue[800],

        // Define the default font family.
        fontFamily: 'Fredoka One',

        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: const TextTheme(
          headline6: TextStyle(fontSize: 24.0),
        ),
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Tes Covid'),
          backgroundColor: Colors.blueGrey,
          actions: [
            // Navigate to the Search Screen
            IconButton(
                onPressed: () => Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => SearchPage())),
                icon: Icon(Icons.search)
            )
          ],
        ),
        backgroundColor: Colors.white,
        body: Container(
          child: MyCardWidget(),
        ),
        //https://api.flutter.dev/flutter/material/FloatingActionButton-class.html
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const MyCustomForm()),
            );
          },
          child: const Icon(Icons.add),
          backgroundColor: Colors.blueGrey,
        ),
        //https://material.io/components/navigation-drawer/flutter#standard-navigation-drawer
        drawer: const MyDrawer(),
      ),
    );
  }
}

List<TesCovid> covidTest = [
  TesCovid(
      title: 'Tes Rapid RS Hermina',
      location: 'Bekasi Barat',
      url: "https://www.rs-jih.co.id/images/news/img_kduMnZy.jpg",
      price: 'Rp100.000'),
  TesCovid(
      title: 'Swab Antigen RS UI',
      location: 'Universitas Indonesia, Depok',
      url:
      "https://res.cloudinary.com/dk0z4ums3/image/upload/v1619681195/attached_image/rapid-test-antibodi-ini-yang-harus-anda-ketahui.jpg",
      price: 'Rp200.000'),
  TesCovid(
      title: 'PCR Drive Thru Mitra Keluarga',
      location: 'Cibubur, Jakarta Timur',
      url:
      "https://mitrakeluarga.s3.ap-southeast-1.amazonaws.com/images/promo/full/396_66_drive-thru-tes-covid-19-pcr-antigen.jpg",
      price: 'Rp300.000')
];

Widget covidTestTemplate(covidTest) {
  return Card(
    shadowColor: Colors.black38,
    margin: const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
    child: Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8.0),
              topRight: Radius.circular(8.0),
            ),
            child: Image.network(covidTest.url,
                width: 150, height: 160, fit: BoxFit.cover),
          ),
          SizedBox(height: 6.0),
          Text(
            covidTest.title,
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.grey[800],
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 6.0),
          Text(
            covidTest.location,
            style: TextStyle(
              fontSize: 14.0,
              color: Colors.grey[500],
            ),
          ),
          SizedBox(height: 15.0),
          Text(
            covidTest.price,
            style: TextStyle(
              fontSize: 30.0,
              color: Colors.red[800],
              fontWeight: FontWeight.bold,
            ),
          ),
          OutlinedButton(
            style: TextButton.styleFrom(
              primary: Colors.blue,
              onSurface: Colors.red,
            ),
            onPressed: () => { print("ini tombol") },
            child: Text('Lebih Lengkap'),
          )
        ],
      ),
    ),
  );
}

/// This is the stateless widget that the main application instantiates.
class MyCardWidget extends StatelessWidget {
  const MyCardWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: list + list2,
      ),
    );
  }
}

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          const Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(
                'LindungPeduli',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                )
            ),
          ),
          const Divider(
            height: 1,
            thickness: 1,
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            // selected: _selectedDestination == 0,
            // onTap: () => selectDestination(0),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.medical_services),
            title: Text('Daftar Vaksin'),
            // selected: _selectedDestination == 1,
            // onTap: () => selectDestination(1),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.medication),
            title: Text('Kamus Obat'),
            // selected: _selectedDestination == 2,
            // onTap: () => selectDestination(2),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.local_pharmacy),
            title: Text('Apotek Daring'),
            // selected: _selectedDestination == 2,
            // onTap: () => selectDestination(2),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.article),
            title: Text('Artikel'),
            // selected: _selectedDestination == 2,
            // onTap: () => selectDestination(2),
            onTap: () => {},
          ),
          const Divider(
            height: 1,
            thickness: 1,
          ),
          const Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(
              'Akun',
            ),
          ),
          ListTile(
            leading: Icon(Icons.login),
            title: Text('Masuk'),
            // selected: _selectedDestination == 3,
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.app_registration),
            title: Text('Daftar'),
            // selected: _selectedDestination == 3,
            onTap: () => {},
          ),
        ],
      ),
    );
  }
}

// https://stackoverflow.com/questions/48927928/how-to-add-clear-button-to-textfield-widget
var _controller = TextEditingController();

// https://www.kindacode.com/article/flutter-add-a-search-field-to-the-app-bar/
class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 60,
        // The search area here
        backgroundColor: Colors.blueGrey,
        title: Container(
          width: double.infinity,
          height: 40,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(5)),
          child: Center(
            child: TextField(
              controller: _controller,
              onChanged: (text) {
                print('First text field: $text');
              },
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search, color: Colors.blueGrey),
                  suffixIcon: IconButton(
                    icon: Icon(Icons.clear, color: Colors.blueGrey),
                    onPressed: _controller.clear,
                  ),
                  hintText: 'Cari...',
                  border: InputBorder.none),
            ),
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(50.0),
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(height: 40),
              Text("Hasil Pencarian: "),
              TextField(
                enabled: false,
                controller: _controller,
              )
            ]
          )
        ),
      ),
    );
  }
}
