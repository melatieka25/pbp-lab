
import 'package:flutter/cupertino.dart';

class TesCovid {
  String? title;
  String? location;
  String? url;
  String? price;

  TesCovid({this.title, this.location, this.url, this.price});
}
