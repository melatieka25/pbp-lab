1. Apakah perbedaan antara JSON dan XML?
Berdasarkan pemahaman saya setelah mempelajari dan mengerjakan To-Do lab 2, saya menemukan bahwa ternyata terdapat beberapa perbedaan signifikan di antara JSON dan XML. JSON adalah JavaScript Object Notation yang tentunya menggunakan bahasa JavaScript, sedangkan XML adalah eXtensible Markup Language yang merupakan turunan dari SGML (Standard Generalized Markup Language). Dari penampilannya JSON memuat data-data yang ada ke dalam bentuk Object yang mirip dengan Dictionary pada Python atau Maps pada Java, sedangkan XML memuat data-data yang tersimpan di dalam tag, sehingga terdapat tree atau percabangan dari tag di dalamnya.

Menurut saya, JSON lebih mudah digunakan dan dibaca karena sintaksnya sama dengan sintaks java, sedangkan XML terlalu panjang dan rumit karena menggunakan tag beserta dengan atributnya untuk merepresentasikan suatu data. Didapat dari Geeksforgeeks.org, berbedaan lain dari keduanya adalah XML lebih aman daripada JSON, XML bisa menggunakan namespaces dan comment sementara XML tidak, dan XML memungkinkan semua tipe encoding sedangkan JSON hanya memungkinkan tipe encoding UTF-8 saja.

2. Apakah perbedaan antara HTML dan XML?
Seperti yang telah dipelajari pada kelas kemarin, HTML atau Hypertext Markup Language adalah sebuah perpaduan antara Hypertext dan markup language. Hypertext pada HTML digunakan untuk menjadi penghubung antara situs web yang satu dengan yang lain. Markup language pada HTML dapat digunakan untuk membuat situs web yang statis karena HTML hanya akan menampilkan data bukan mentransfer data. HTML juga akan memuat struktur dari situs web yang akan kita buat.

Di sisi lain, XML atau eXtensible Markup Language merupakan sebuah markup language yang dapat digunakan untuk membuat situs web yang dinamis karena XML dapat langsung mentransfer data. XML dan HTML keduanya menggunakan tag, namun tag pada XML dapat dikustomisasi sesuai dengan bahasa yang dipakai oleh pembuat program, berbeda dengan HTML yang tagnya sudah didefinisikan secara umum terlebih dahulu. Kita bisa saja membuat tag <dari> atau <untuk> ketika membuat halaman web dengan menggunakan XML. XML bersifat case sensitive sedangkan HTML tidak. Pada XML, closing tags sangat dibutuhkan, berbeda dengan HTML yang menganggap bahwa closing tags tidak terlalu penting.

Referensi:
Lab-2 PBP
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://www.upgrad.com/blog/html-vs-xml/